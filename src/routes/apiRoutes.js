const router = require('express').Router()
const teacherController = require("../controller/teacherController")
const JSONPlaceholderController = require ('../controller/JSONPlaceholderController');
const dbController = require('../controller/db.controller');
const pizzariaController = require('../controller/pizzaria.controller');



router.get('/teacher/', teacherController.getAlgumaCoisa);

router.post('/cadastroAluno/:_id', teacherController.postAluno);

router.put('/updateAluno/:_id', teacherController.updateAluno);

router.delete('/deleteAluno/', teacherController.deleteAluno);

// router.get('/alunos', teacherController.getAlunos);

router.get('/external/', JSONPlaceholderController.getUsers);
router.get('/external/io', JSONPlaceholderController.getUsersWebsiteIO);
router.get('/external/com', JSONPlaceholderController.getUsersWebsiteCOM);
router.get('/external/net', JSONPlaceholderController.getUsersWebsiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);


// Teste DB

router.get('/getUser/', dbController.getUsers);


// Pizzaria DB

router.get('/getPizzas/', pizzariaController.getPizzas);
router.post('/createPizza/', pizzariaController.createPizza);

module.exports = router;