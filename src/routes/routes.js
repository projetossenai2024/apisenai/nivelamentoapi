const router = require('express').Router();
const dbController = require('../controller/db.controller');
const eventosController = require('../controller/eventos.controller');
const pizzariaController = require('../controller/pizzaria.controller');

// Teste DB
router.get('/getUser/', dbController.getUsers);

// Pizzaria DB
router.post('/createPizza/', pizzariaController.createPizza);
router.get('/getPizzas/', pizzariaController.getPizzas);
router.get('/getPizzaById/:id', pizzariaController.getPizzaById);

// Eventos DB
router.get('/getTables/', eventosController.getTables);
router.get('/descCompra/', eventosController.descCompra);
router.get('/descIngresso/', eventosController.descIngresso);
router.get('/descOrganizador/', eventosController.descOrganizador);
router.get('/descUsuario/', eventosController.descUsuario);

module.exports = router;
