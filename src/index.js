const express = require('express');
const app = express();
const cors = require('cors');
const apiRoutes = require('./routes/routes');
const db = require('../db/conn')
const testConnect = require('../db/testConnect');


class AppController {
    constructor() {
        this.express = express();
        this.middlewares();
        this.routes();
    }

    middlewares() {
        this.express.use(express.json());
        this.express.use(cors());
    }

    routes() {
        this.express.use('/api', apiRoutes); //requere somente a rota api
        this.express.get("/health/", (_, res) => {
            res.send({ status: "Teste" });
          });
        this.express.get('/test-connect/', (_, res) => {
            testConnect(res);
          });
    }
}

module.exports = new AppController().express;
