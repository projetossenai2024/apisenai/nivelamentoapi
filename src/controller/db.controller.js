const db = require('../../db/conn')

class dbController {

    static async getUsers(req, res) {

        try {
            const users = await db.query('SELECT * FROM user;')

            return res.status(200).json({ message: 'Esses são os usuários cadastrados', users })
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erro ao requerir os usuários' + error.message })
        }
    }

    static async getTables(req, res) {
        try {
            const tables = await db.query(
                'SELECT * FROM tables WHERE table_schema = ?', ['db']
            ); // Query para buscar os nomes das tabelas no banco de dados
    
            if (!tables || tables.length === 0) {
                return res.status(404).json({ error: 'Nenhuma tabela encontrada.' });
            }
    
            const tableNames = tables.map(table => table.table_name); // Extrai apenas os nomes das tabelas
    
            return res.status(200).json({ message: 'Essas são as tabelas do banco:', tables: tableNames }); // Retorna uma mensagem e os nomes das tabelas cadastradas
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erro ao obter as tabelas do Banco de Dados: ' + error.message });
        }
    }
    


}

module.exports = dbController;