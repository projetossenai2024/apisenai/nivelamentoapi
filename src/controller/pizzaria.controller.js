const db = require('../../db/conn');

class pizzariaController {

    static async createPizza(req, res) {
        const { nome, descricao, valor, tamanho } = req.body;
        const errors = {};

        try {
            if(!nome || typeof nome !== 'string'){
                errors.nome = ['Nome is required']
            }

            if(!descricao || typeof descricao !== 'string'){
                errors.nome = ['descricao is required']
            }
            
            if(!tamanho || typeof tamanho !== 'string'){
                errors.nome = ['tamanho is required']
            }


            if (Object.keys(errors).length > 0) {
                return res.status(422).json({ message: 'Invalid properties', errors });
            }

            const pizza = await db.query(
                'INSERT INTO pizza (nome, descricao, valor, tamanho) VALUES (?, ?, ?, ?);',
                [nome, descricao, valor, tamanho]
            );

            return res.status(200).json({ message: 'Pizza cadastrada com sucesso:', pizza })
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erro ao listar pizzas cadastradas' })
        }
    }

    static async getPizzas(req, res) {
        try {
            const pizzas = await db.query('SELECT * FROM pizza;')

            return res.status(200).json({ message: 'Essas são as pizzas cadastradas:', pizzas })

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erro ao listar pizzas cadastradas' })
        }
    }

    static async getPizzaById(req, res) {
        const { id_pizza } = req.params;
    
        // Validar se id_pizza é um número
        if (isNaN(id_pizza)) {
            return res.status(400).json({ error: 'ID de pizza inválido' });
        }
    
        try {
            const pizza = await getPizzaFromDatabase(id_pizza);
    
            if (!pizza || pizza.length === 0) {
                return res.status(404).json({ error: 'Pizza não encontrada' });
            }
    
            return res.status(200).json({ message: 'Esta é a pizza cadastrada:', pizza: pizza[0] });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erro ao listar pizzas cadastradas' });
        }
    }
    
    // Função auxiliar para consultar a pizza no banco de dados
    static async getPizzaFromDatabase(id_pizza) {
        const pizzas = await db.query('SELECT * FROM pizza WHERE id_pizza = ?', [id_pizza]);
        return pizzas;
    }
}

module.exports = pizzariaController;