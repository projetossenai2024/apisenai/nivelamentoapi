class TeacherController {
    // Defina a variável alunos em escopo da classe
    static alunos = [];

    static async getAlgumaCoisa(req, res) {
        res.status(200).json({
            algumaMensagem: {
                algumaMensagem: 'Igor',
                algumaNumero: 17,
            }
        });
    }

    static async postAluno(req, res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        const { _id } = req.params;

        // Acesso à variável alunos diretamente do escopo da classe
        const novoAluno = {
            nome,
            idade,
            profissao,
            cursoMatriculado,
            _id
        }

        TeacherController.alunos.push(novoAluno); // Usando a classe para acessar a variável alunos
        console.log('Aluno cadastrado com sucesso:', novoAluno);

        res.status(200).json({ message: 'Aluno cadastrado com sucesso!' })
    }

    // static async getAluno(req, res){
    //     res.status(200).json({alunos: TeacherController.alunos});
    // }

    static async updateAluno(req, res) {
        const { nome, idade, profissao, cursoMatriculado } = req.body;
        const { _id } = req.params;

        const alunoIndex = TeacherController.alunos.findIndex(aluno => aluno._id === _id);

        if (alunoIndex === -1) {
            return res.status(404).json({ message: "Aluno não encontrado." });
        }

        // Mesclar os dados do aluno existente com os novos dados fornecidos
        Object.assign(TeacherController.alunos[alunoIndex], {
            nome: nome ?? TeacherController.alunos[alunoIndex].nome,
            idade: idade ?? TeacherController.alunos[alunoIndex].idade,
            profissao: profissao ?? TeacherController.alunos[alunoIndex].profissao,
            cursoMatriculado: cursoMatriculado ?? TeacherController.alunos[alunoIndex].cursoMatriculado
        });

        console.log("Aluno atualizado:", TeacherController.alunos[alunoIndex]);
        res.status(200).json({ message: "Aluno atualizado com sucesso." });
    }

    static async deleteAluno(req, res) {
        const { id } = req.query;

        // Encontrar o índice do aluno na array alunos
        const alunoIndex = TeacherController.alunos.findIndex(aluno => aluno._id === id);
    
        // Se não encontrar o aluno, retorna um erro 404
        if (alunoIndex === -1) {
            return res.status(404).json({ message: "Aluno não encontrado." });
        }
    
        // Remove o aluno da array alunos
        TeacherController.alunos.splice(alunoIndex, 1);
    
        console.log("Aluno removido:", id);
        res.status(200).json({ message: "Aluno removido com sucesso." });
    }
}

module.exports = TeacherController;
