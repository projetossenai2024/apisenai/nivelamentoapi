const db = require('../../db/conn')

function filtrarResposta(resposta) {
    // Verifique se a resposta é um objeto
    if (!resposta || typeof resposta !== 'object') {
        return resposta;
    }

    // Remova o buffer e outras propriedades irrelevantes
    const propriedadesParaRemover = ['_buf', '_clientEncoding', '_catalogLength', '_catalogStart', '_schemaLength', '_schemaStart', '_tableLength', '_tableStart', '_orgTableLength', '_orgTableStart', '_orgNameLength', '_orgNameStart', 'characterSet', 'encoding', 'name', 'columnLength', 'columnType', 'type', 'flags', 'decimals', '{}'];
    propriedadesParaRemover.forEach((propriedade) => {
        if (Array.isArray(resposta)) {
            resposta = resposta.map((item) => {
                delete item[propriedade];
                return filtrarResposta(item);
            });
        } else {
            delete resposta[propriedade];
        }
    });

    // Retorne a resposta filtrada
    return resposta;
}

class eventosController {

    static async getTables(req, res) {
        try {
            const tables = await db.query(
                'SHOW TABLES'
            );

            // Extrair apenas os nomes das tabelas
            const tableNames = filtrarResposta(tables);

            return res.status(200).json({ message: 'Estas são as tabelas:', tableNames });

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Não foi possível conectar à base de dados SQL.', error });
        }
    }

    static async descCompra(req, res) {
        try {
            const compra = await db.query(
                'desc compra'
            );

            const respostaFiltrada = filtrarResposta(compra);

            return res.status(200).json({ message: 'Resultado', respostaFiltrada });

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Não foi possível conectar à base de dados SQL.', error });
        }
    }

    static async descIngresso(req, res) {
        try {
            const ingresso = await db.query(
                'desc ingresso'
            );

            const respostaFiltrada = filtrarResposta(ingresso);

            return res.status(200).json({ message: 'Resultado', respostaFiltrada });

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Não foi possível conectar à base de dados SQL.', error });
        }
    }

    static async descOrganizador(req, res) {
        try {
            const organizador = await db.query(
                'desc organizador'
            );

            const respostaFiltrada = filtrarResposta(organizador);

            return res.status(200).json({ message: 'Resultado', respostaFiltrada });

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Não foi possível conectar à base de dados SQL.', error });
        }
    }

    static async descUsuario(req, res) {
        try {
            const usuario = await db.query(
                'desc usuario'
            );

            const respostaFiltrada = filtrarResposta(usuario);

            return res.status(200).json({ message: 'Resultado', respostaFiltrada });

        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Não foi possível conectar à base de dados SQL.', error });
        }
    }

}

module.exports = eventosController;