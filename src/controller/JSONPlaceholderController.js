const axios = require('axios');

module.exports = class JSONPlaceholderController {
    static async getUsers(req, res) {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users/');
            const users = response.data;
            res.status(200).json({ message: 'Aqui estão estão os usuários requisitados pela API pública JSONPlaceholder', users });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: 'Erro ao buscar dados do JSONPlaceholder' });
        }
    }

    static async getUsersWebsiteIO(req, res) {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users/');
            const users = response.data.filter(
                (user) => user.website && user.website.endsWith(".io")
            );
            const banana = users.length;
            res.status(200).json({ message: 'Aqui estão estão os usuários requisitados pela API pública JSONPlaceholder. Quantos usuários tem esse dominio:', banana, users });

        } catch (error) {
            console.log(error);
            res.status(500).json({ message: 'Erro ao buscar dados do JSONPlaceholder' });
        }
    }

    static async getUsersWebsiteCOM(req, res) {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users/');
            const users = response.data.filter(
                (user) => user.website && user.website.endsWith(".com")
            );
            const banana = users.length;
            res.status(200).json({ message: 'Aqui estão estão os usuários requisitados pela API pública JSONPlaceholder. Quantos usuários tem esse dominio:', banana, users });

        } catch (error) {
            console.log(error);
            res.status(500).json({ message: 'Erro ao buscar dados do JSONPlaceholder' });
        }
    }

    static async getUsersWebsiteNET(req, res) {
        try {
            const response = await axios.get('https://jsonplaceholder.typicode.com/users/');
            const users = response.data.filter(
                (user) => user.website && user.website.endsWith(".net")
            );
            const banana = users.length;
            res.status(200).json({ message: 'Aqui estão estão os usuários requisitados pela API pública JSONPlaceholder. Quantos usuários tem esse dominio:', banana, users });

        } catch (error) {
            console.log(error);
            res.status(500).json({ message: 'Erro ao buscar dados do JSONPlaceholder' });
        }
    }

    static async getCountDomain(req, res) {
        const { dominio } = req.query;

        if (!dominio) {
            res.status(400).json({ message: "O domínio precisa ser informado." });
            return;
        }

        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users/");
            const users = response.data.filter(
                (user) => user.website && user.website.endsWith(dominio)
            );

            const quantidadeUsuarios = users.length;

            res
                .status(200)
                .json({
                    message: `Quantidade de usuários com o domínio ${dominio}:`,
                    quantidadeUsuarios,
                    users,
                });
        } catch (error) {
            console.log(error);
            res.status(500).json({ message: "Erro ao buscar dados do JSONPlaceholder" });
        }
    }

};